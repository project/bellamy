/*
 * Bellamy
 * JS code
 */

// Fonts
require('./fonts.css')

// W3.CSS
require('./w3.css')

// CommonJS module
const m = require('./module')

// Main
console.log('Bellamy JS code')

window.onload = function () {
  if (window.jQuery) {
    console.log('jQuery is loaded :(')
    const $jq = window.jQuery.noConflict()
    $jq(document).ready(function () {
      m.load()
    })
  } else {
    console.log('Good news! jQuery is not loaded')
    m.load()
  }
}
