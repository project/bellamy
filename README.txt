
ABOUT BELLAMY
-------------

Bellamy is a Drupal sub-theme, derived from Stable9, to which Bellamy adds a
very simple stylesheet and a CommonJS module that contains all the 
interesting code.
The other JavaScript libraries are requested and put together via browserify.
There is a package.json file in the bellamy/js folder that sets up the 
commands needed to build the bundle.js file, which is imported automatically.
The node.js stack and npm packet manager are required to build the bundle.js
file. First, you have to install the dependencies:

npm install

And then:

npm run-script build

If you make changes to the JavaScript code, you can use a linter:

npm run-script lint

or

npm run-script fix

In summary, the JavaScript module manipulates the DOM, replacing some parts 
of the original HTML code. CSS classes are also applied, based on the 
configuration in bellamy/js/src/config.
For convenience, the configuration is divided into three JSON files, one for
theme-specific classes, one for W3.CSS (a very lightweight CSS framework),
and one for Font Awesome icons.
JSON configuration files have the following format:

{
  "selector": ".layout-container, .layout-container h1",
  "classes": ["reset-margin", "normal-font"]
}

ABOUT STABLE 9
--------------

Stable allows core markup and styling to evolve by functioning as a backwards
compatibility layer for themes against changes to core markup and CSS. If you
browse Stable's contents, you will find copies of all the Twig templates and
CSS files provided by core.

ABOUT DRUPAL THEMING
--------------------

For more information, see Drupal.org's theming guide.
https://www.drupal.org/docs/theming-drupal
